<?php 

    /*
     * Template Name: Home
     * Theme Name: Jaguar Team
     * Theme URI: http://someURI/home
     * Author: Pervozdanniy
     * Author URI: http://jaguar-team.com.ua
     *
     */
?>
<?php get_header(); ?>    
        <section class="main">
            <div class="container top-slider">
                <div class="container">
                    <img src="<?php echo get_template_directory_uri().'/img/main-img.png'?>" alt="ipad">
                    <div class="main-description">
                        <h2>Другие накручивают подписчиков, мы привлекаем клиентов</h2>
                        <p>3 дня тест-драйва + аудит аккаунта бесплатно</p>
                        <button class="btn">тест драйв</button>
                    </div>
                </div>
                <div></div>
            </div>
        </section>
        <section class="about-us">
            <div class="container">
                <h3>О нас</h3>
                <div class="flex-container">
                    <div class="video">
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </div>
                    <div class="about-us-description">
                        <p>Мы занимаемся привлечением реальных пользователей посредством увеличения активности вашей страницы.  Мы делаем то, что вы обычно делаете с телефона, только в несколько сот раз быстрее (порядка 3000 действий в сутки), таким образом создаем максимальную активность Вашего аккаунта и пользователи самостоятельно подписываются на  Ваш аккаунт как результат нашей работы.</p>
                        <p>Вы выбираете тариф и нажав  кнопку "заказать" заполняете заявку, ниже есть пользовательское соглашение - договор между нами. После заполнения заявки вы попадаете на страницу с реквизитами для оплаты.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="why">
            <div class="container">
                <h4>Почему мы</h4>
                <ul>
                    <li>
                        <span class="geo fa fa-dot-circle-o"></span>
                        <h5>Таргетинг по гео</h5>
                        <p>Целевая аудитория в INSTAGRAM по гео-локации.</p>
                    </li>
                    <li>
                        <span class="filter fa fa-child"></span>
                        <h5>Фильтрация по полу</h5>
                        <p>Целевая аудитория в INSTAGRAM по полу</p>
                    </li>
                    <li>
                        <span class="hashtag fa fa-hashtag"></span>
                        <h5>Хештеги</h5>
                        <p>Целевая аудитория в INSTAGRAM по хештегам</p>
                    </li>
                    <li>
                        <span class="base fa fa-archive"></span>
                        <h5>Базы конкурентов</h5>
                        <p>Анализ и привлечение в INSTAGRAM активной аудитории Ваших конккурентов</p>
                    </li>
                </ul>
                <button class="btn btn2">заказать  сейчас</button>
            </div>
        </section>
        <section class="develop">
            <div class="container">
                <h3>Почему стоит продвигать инст</h3>
                <ul>
                    <li>
                        <span class="users"></span>
                        <h5>Более 400 миллионов пользователей</h5>
                        <p>из которых 15% русскоговорящие</p>
                    </li>
                    <li>
                        <span class="competition"></span>
                        <h5>Конкуренция в 15 раз ниже чем</h5>
                        <p>в других соц сетях и в 35 раз меньше чем в  Яндексе</p>
                    </li>
                    <li>
                        <span class="apple"></span>
                        <h5>50% пользователей</h5>
                        <p>владельцы техники Apple</p>
                    </li>
                    <li>
                        <span class="minutes"></span>
                        <h5>25 минут в день в среднем</h5>
                        <p>проводит каждый пользователь в INSTAGRAM</p>
                    </li>
                    <li>
                        <span class="business"></span>
                        <h5>Подходит для 80% бизнеса</h5>
                        <p>где есть услуга и или товар</p>
                    </li>
                    <li>
                        <span class="grow"></span>
                        <h5>Ежемесячный  прирост пользователей</h5>
                        <p>более 20миллионов</p>
                    </li>
                    <li>
                        <span class="shop"></span>
                        <h5>70% пользователей Instagram</h5>
                        <p>совершает покупки в интернете</p>
                    </li>
                    <li>
                        <span class="level"></span>
                        <h5>Уровень вовлеченности в 15 раз</h5>
                        <p>выше чем в других соц сетях</p>
                    </li>
                    <li>
                        <span class="age"></span>
                        <h5>Средний возраст пользователя</h5>
                        <p>до 35 лет</p>
                    </li>
                </ul>
            </div>
        </section>
        <section class="for">
            <div class="container">
                <h3>Для кого это</h3>
                <ul>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/entrepreneurs.png'?>" alt="entrepreneurs" class="entrepreneurs">
                        <h5>Предприниматели</h5>
                        <p>Привлекаете новых подписчиков и выстраиваете канал, приносящий прибыль</p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/without.png'?>" alt="without" class="without">
                        <h5>Без профессии</h5>
                        <p>Строите новые источники дохода и занимаете свою нишу</p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/housewives.png'?>" alt="housewives" class="housewives">
                        <h5>Домохозяйки</h5>
                        <p>Получаете возможность заниматься любимым делом еще и на этом зарабатывать</p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/beautifuls.png'?>" alt="beautifuls" class="beautifuls">
                        <h5>Красивые</h5>
                        <p>Только делаешь селфи и получаешь приглашения на самые крутые тусовки</p>
                    </li>
                </ul>
                <button class="btn">заказать  сейчас</button>
            </div>
        </section>
        <section class="how-it-work">
            <div class="container">
                <h3>Как мы работаем</h3>
                <div class="how-it-work-container">
                    <ul class="steps">
                        <li class="audit">
                            <a href="index.php#audit"><span class="fa fa-bar-chart"></span></a>
                            <a href="index.php#audit">Аудит аккаунта</a>
                        </li>
                        <li class="analysis">
                            <a href="index.php#analysis"><span class="fa fa-search"></span></a>
                            <a href="index.php#analysis">Анализ ЦА</a>
                        </li>
                        <li class="order">
                            <a href="index.php#order"><span class="fa fa-paper-plane"></span></a>
                            <a href="index.php#order">Заявка</a>
                        </li>
                        <li class="pay">
                            <a href="index.php#pay"><span class="fa fa-rub"></span></a>
                            <a href="index.php#pay">Оплата</a>
                        </li>
                        <li class="start">
                            <a href="index.php#start"><span class="fa fa-rocket"></span></a>
                            <a href="index.php#start">Запуск</a>
                        </li>
                    </ul>
                    <ul class="steps-description">
                        <li id="audit">
                            <span class="fa fa-bar-chart"></span>
                            <h5>Анализ ЦА</h5>
                            <p>Планирование каждой рекламной кампании и индивидуальный подход позволяют действительно увеличить конверсию, а так же существенно повысить продажи.</p>
                        </li>
                        <li id="analysis">
                            <span class="fa fa-search"></span>
                            <h5>Анализ ЦА</h5>
                            <p>Планирование каждой рекламной кампании и индивидуальный подход позволяют действительно увеличить конверсию, а так же существенно повысить продажи.</p>
                        </li>
                        <li id="order">
                            <span class="fa fa-paper-plane"></span>
                            <h5>Анализ ЦА</h5>
                            <p>Планирование каждой рекламной кампании и индивидуальный подход позволяют действительно увеличить конверсию, а так же существенно повысить продажи.</p>
                        </li>
                        <li id="pay">
                            <span class="fa fa-rub"></span>
                            <h5>Анализ ЦА</h5>
                            <p>Планирование каждой рекламной кампании и индивидуальный подход позволяют действительно увеличить конверсию, а так же существенно повысить продажи.</p>
                        </li>
                        <li id="start">
                            <span class="fa fa-rocket"></span>
                            <h5>Анализ ЦА</h5>
                            <p>Планирование каждой рекламной кампании и индивидуальный подход позволяют действительно увеличить конверсию, а так же существенно повысить продажи.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="trust">
            <div class="container">
                <h3>Нам доверяют</h3>
                <ul class="brand-slider">
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/tvoe.jpg'?>" alt="tvoe">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/oodji.jpg'?>" alt="oodji">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/ecco.jpg'?>" alt="ecco">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/mango.jpg'?>" alt="mango">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/oliwer.jpg'?>" alt="oliwer">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/finn-flare.jpg'?>" alt="finn flare">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/tvoe.jpg'?>" alt="tvoe">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/oodji.jpg'?>" alt="oodji">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/ecco.jpg'?>" alt="ecco">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/mango.jpg'?>" alt="mango">
                    </li>
                </ul>
                <ul class="brand-rewiew">
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                    <li>
                        <video src="<?php echo get_template_directory_uri().'/video/create.mp4'?>" controls></video>
                    </li>
                </ul>
            </div>
        </section>
        <section class="price">
            <div class="container">
                <h3>Цены</h3>
                <ul>
                    <li>
                        <div class="plan">
                            <p class="rate">мини</p>
                            <div>
                                <p class="plan-price">900 грн</p>
                                <p class="during">в месяц</p>
                            </div>
                            <p class="users"><span>1500</span>подписчиков</p>
                            <button class="btn">заказать  сейчас</button>
                        </div>
                    </li>
                    <li>
                        <div class="plan">
                            <p class="rate">стандарт</p>
                            <div>
                                <p class="plan-price">900 грн</p>
                                <p class="during">в месяц</p>
                            </div>
                            <p class="users"><span>1500</span>подписчиков</p>
                            <button class="btn">заказать  сейчас</button>
                        </div>
                    </li>
                    <li class="top">
                        <div class="plan">
                            <p class="rate">бизнес</p>
                            <div>
                                <p class="plan-price">900 грн</p>
                                <p class="during">в месяц</p>
                            </div>
                            <p class="users"><span>1500</span>подписчиков</p>
                            <button class="btn">заказать  сейчас</button>
                        </div>
                    </li>
                    <li>
                        <div class="plan">
                            <p class="rate">премиум</p>
                            <div>
                                <p class="plan-price">900 грн</p>
                                <p class="during">в месяц</p>
                            </div>
                            <p class="users"><span>1500</span>подписчиков</p>
                            <button class="btn">заказать  сейчас</button>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="sum">
            <img src="<?php echo get_template_directory_uri().'/img/hand.png'?>" alt="hand">
            <div class="container">
                <p>Нет всей суммы?</p>
                <p>Узнай как можно оплатить частями!</p>
                <button class="btn btn2">узнать сейчас</button>
            </div>
        </section>
        <section class="our-team">
            <div class="container">
                <h3>Команда</h3>
                <ul>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/person1.jpg'?>" alt="Фото члена команды">
                        <h4>Дмитрий Прилипко</h4>
                        <p>менеджер по продажам</p>
                        <p>т. 063 43 27 830</p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/person2.jpg'?>" alt="Фото члена команды">
                        <h4>Дмитрий Прилипко</h4>
                        <p>менеджер по продажам</p>
                        <p>т. 063 43 27 830</p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/person3.jpg'?>" alt="Фото члена команды">
                        <h4>Татьяна Штука</h4>
                        <p>менеджер по продажам</p>
                        <p>т. 063 43 27 830</p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/person4.jpg'?>" alt="Фото члена команды">
                        <h4>Дмитрий Прилипко</h4>
                        <p>менеджер по продажам</p>
                        <p>т. 063 43 27 830</p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri().'/img/person2.jpg'?>" alt="Фото члена команды">
                        <h4>Дмитрий Прилипко</h4>
                        <p>менеджер по продажам</p>
                        <p>т. 063 43 27 830</p>
                    </li>
                </ul>
                <button class="btn">наши вакансии</button>
            </div>
        </section>
        <section class="blog">
            <div class="container">
                <h3>Блог</h3>
                <div class="articles">
                    <?php $posts = get_posts(["orderby" => "date", "numberposts" => "3"]); ?>
                    <?php foreach ($posts as $key => $value): ?>
                    <?php
                        $id_post = $value->ID;
                        $img_src = get_the_post_thumbnail($id_post);
                        $user_info = get_userdata($value->post_author); 
                    ?>
                        <div class="post">
                            <a href="<?= get_permalink($id_post); ?>" class="thumbnail">
                                <img src="<?= $img_src; ?>" alt="Миниатюра">
                            </a>
                            <article>
                                <div class="post-info">
                                    <h4><a href="index.php#"><?= $value->post_title ?></a></h4>
                                    <p>автор: <?= $user_info->nickname; ?><span><?= get_date_from_gmt( $value->post_date, 'j F Y' ) ?></span></p>
                                </div>
                                <p><?= crop_string($value->post_content, 150, '...'); ?></p>
                            </article>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <div class="panel panel-default modal">
            ger ilwfhwioeh fewgiowhfw;oih f;owfhwihf; hfew;h fewihf;weh hgirh gi;h;ahg;eg fu
        </div>
    <?php get_footer(); ?>