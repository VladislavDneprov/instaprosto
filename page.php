<?php get_header(); ?>

	<div class="main">
		<div class="container">
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
				
				the_content();

			// End the loop.
			endwhile;
			?>
		</div>
	</div>

<?php get_footer(); ?>