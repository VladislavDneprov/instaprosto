		<button class="btn back-contact">Обратная связь</button>
        <div id="modal-background" modal-backdrop="" style="z-index: 1040;"></div>
        <section class="contact">
            <div class="container">
                <p class="logo"><a href="<?= get_home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/img/logo.png'?>" alt="logo"></a></p>
                <ul class="menu">
                    <li>
                        <a href="index.php">Главная</a>
                    </li>
                    <li>
                        <a href="vacancy.php#">База знаний</a>
                    </li>
                    <li>
                        <a href="blog.php">Блог</a>
                    </li>
                    <li>
                        <a href="vacancy.php#">Команда</a>
                    </li>
                </ul>
                <ul class="menu">
                    <li>
                        <a href="vacancy.php#">Цены</a>
                    </li>
                    <li>
                        <a href="vacancy.php#">Контакты</a>
                    </li>
                    <li>
                        <a href="vacancy.php#">Поддержка</a>
                    </li>
                    <li>
                        <a href="vacancy.php">Вакансии</a>
                    </li>
                    <li>
                        <a href="vacancy.php#">Доп услуги</a>
                    </li>
                </ul>
                <div class="contact-info">
                    <h5>Мы в сооциальных сетях</h5>
                    <ul class="social">
                        <li>
                            <a href="vacancy.php#" class="fa fa-facebook"></a>
                        </li>
                        <li>
                            <a href="vacancy.php#" class="fa fa-vk"></a>
                        </li>
                        <li>
                            <a href="vacancy.php#" class="fa fa-odnoklassniki"></a>
                        </li>
                        <li>
                            <a href="vacancy.php#" class="fa fa-twitter"></a>
                        </li>
                        <li>
                            <a href="vacancy.php#" class="fa fa-instagram"></a>
                        </li>
                    </ul>
                    <h5>Наш телефон</h5>
                    <address>0 800 000 00 00</address>
                </div>
                <p><a href="vacancy.php#">Пользовательское соглашение</a><a href="vacancy.php#">Политика конфиденциальности</a></p>
            </div>
        </section>

		<footer>
		    <div class="container">
		        <p>Профессиональное продвижение instagram.</p>
		        <p>©  "InstaPROsto", 2015-2016. Все права защищены. ФЛП Хабрат С.А.</p>
		    </div>
		</footer>
		<?php wp_footer(); ?>
        <div class="modal-win">
            <div style="text-align:right;"><i class="fa fa-close"></i></div>
            <?php echo do_shortcode('[contact-form-7 id="34" title="Contact form 1"]'); ?>
        </div>
	</body>
</html>