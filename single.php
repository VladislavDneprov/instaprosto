<?php get_header(); ?>

	<div class="main single-page">
		<div class="container">
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();?>
				<div class="full-post">
					<?php the_title( '<h1 class="entry-title">', '</h1>' );?>
				
					<?php the_content(); ?>
				</div>

			
			<?php endwhile;
			// End the loop.
			?>
		</div>
	</div>

<?php get_footer(); ?>