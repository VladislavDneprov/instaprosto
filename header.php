<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>instaprosto</title>
        <?php wp_head(); ?>
  	</head>
	<body>
		 <header>
            <div class="container">
                <address>0 800 000 00 00</address>
                <h1 class="logo">
                    <a href="<?= get_home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/img/logo.png'?>" alt="logo"></a>
                </h1>
                <p class="basket">
                    <div class="my-new-sidebar">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Cart") ) : ?>
                        <?php endif; ?>
                    </div>
                </p>
                <p>
                    <div class="my-new-sidebar">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Currency") ) : ?>
                        <?php endif; ?>
                    </div>
                </p>
            </div>
        </header>
        <nav>
            <?php wp_nav_menu(array('menu' => 'top-menu', 'menu_class' => 'container')); ?>
            <div class="hamb">
                <button class="cmn-toggle-switch cmn-toggle-switch__htx">
                    <span>toggle menu</span>
                </button>
            </div>
        </nav>