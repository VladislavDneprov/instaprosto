jQuery(function($){
	$('.cmn-toggle-switch').click(function(){
        $(this).toggleClass('active');
        $('nav ul').toggleClass('active');
    });

    $('.steps').each(function(){
        var tabLis = $(this).find('li');            
        var tabContent = $('.steps-description li');
        tabContent.hide();
        tabLis.first().addClass('active');
        tabContent.first().show();
    });

    $('.steps').on('click', 'li', function(e) {
        var $this = $(this);
        var parentUL = $this.parent();
        var tabContent = $('.steps-description');

        if($this.hasClass('active')) {
            return false;
        }

        parentUL.children().removeClass('active');
        $this.addClass('active');

        tabContent.find('li').hide();
        var showById = $( $this.find('a').attr('href') );
        tabContent.find(showById).fadeIn(400, function(){
            
        });            

        e.preventDefault();
    });

    $('.brand-slider').slick({
        asNavFor: '.brand-rewiew',
        slidesToShow: 6,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 630,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 460,
                settings: {
                    slidesToShow: 2,
                }
            }
        ]

    });

    $('.brand-rewiew').slick({
        asNavFor: '.brand-slider',
        autoplay: true
    });

   $('.top-slider').slick({
        slidesToShow: 1,
        autoplay: true,
        arrows: true,
    });
   $('button.slick-prev').css({'opacity':1});
   $('button.slick-next').css({'opacity':1});

   

   $('.btn').click(function(){
        if(!$('.modal-win').hasClass('shown')){
            $('.modal-win').addClass('shown').css({'display':'block', 'z-index':'1050'});
            $('body').addClass('modal-open');
            $('#modal-background').addClass('modal-backdrop fade in').css({'display':'block'});
        }
        else {
            $('.modal-win').removeClass('shown').css({'display':'none'});
            $('#modal-background').removeClass('modal-backdrop fade  in').css({'display':'none'});
            $('body').removeClass('modal-open');
        }
   });
   
   $('.basket').mouseover(function(){
        $(this).css({'cursor':'pointer'});
    }).click(function(){
        if(!$('.cart_list').hasClass('shown')){
            $('.cart_list').css({'display':'block'}).addClass('shown');
        }
        else{
            $('.cart_list.product_list_widget').css({'display':'none'}).removeClass('shown');
            
        }
    });
    $('.cart_list.product_list_widget').blur(function(){
        $('.cart_list.product_list_widget').css({'display':'none'}).removeClass('shown');
    });
    $('.fa.fa-close').mouseover(function(){
        $(this).css({'cursor':'pointer'});
    }).click(function(){
        $('.modal-win').removeClass('shown').css({'display':'none'});
        $('#modal-background').removeClass('modal-backdrop fade  in').css({'display':'none'});
        $('body').removeClass('modal-open');
    });
})