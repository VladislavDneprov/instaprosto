<?php 

function enqueue_styles() {

	wp_register_style('normalize', get_template_directory_uri().'/css/normalize.css');
	wp_enqueue_style( 'normalize');

	wp_register_style( 'slick', get_template_directory_uri().'/slick/slick.css' );
	wp_enqueue_style( 'slick' );

	wp_register_style( 'slick-theme', get_template_directory_uri().'/slick/slick-theme.css' );
	wp_enqueue_style( 'slick-theme' );
	
	wp_register_style('font-style', get_template_directory_uri().'/font-awesome-4.6.3/css/font-awesome.min.css');
	wp_enqueue_style( 'font-style');

	wp_register_style('main', get_template_directory_uri().'/css/main.css');
	wp_enqueue_style( 'main');

	wp_register_style('style', get_template_directory_uri().'/style.css');
	wp_enqueue_style( 'style');

}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_scripts () {
	wp_enqueue_script( 'slick-js', get_template_directory_uri().'/slick/slick.min.js');
	wp_enqueue_script( 'effect', get_template_directory_uri().'/js/main.js');
	
}

add_action('wp_enqueue_scripts', 'enqueue_scripts');



/** theme support **/
add_theme_support('menus');
add_theme_support( 'woocommerce' );
add_theme_support( 'post-thumbnails' );

/** add filter **/
add_filter('post_thumbnail_html', 'true_thumbnail_url_only', 10, 5);

/** register sidebar **/
if (function_exists('register_sidebar')) {
	register_sidebar(array(
        'name' => 'Currency',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="title">',
        'after_title' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Cart',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="title">',
        'after_title' => '</div>',
    ));
}

function true_thumbnail_url_only( $html ){
	return preg_replace('#.*src="([^\"]+)".*#', '\1', $html );
}

function crop_string($string, $count, $add_symbol = '')
{  
    return substr($string, 0, $count).((strlen($string) > $count) ? $add_symbol : '');
}

?>