<?php  
/*
     * Template Name: Blog
     * Theme Name: Jaguar Team
     * Theme URI: http://someURI/blog
     * Author: Pervozdanniy
     * Author URI: http://jaguar-team.com.ua
     *
     */

?>
<?php get_header();  ?>
<?php $post = get_posts();?>
		<section class="blog-page">
            <div class="container">
                <h1>Блог</h1>
                <?php $posts = get_posts(["orderby" => "date", "numberposts" => "3"]); ?>
                    <?php foreach ($posts as $key => $value): ?>
                    <?php
                        $id_post = $value->ID;
                        $img_src = get_the_post_thumbnail($id_post);
                        $user_info = get_userdata($value->post_author); 
                    ?>
                        <div class="post">
                            <a href="<?= get_permalink($id_post); ?>" class="thumbnail">
                                <img src="<?= $img_src; ?>" alt="Миниатюра">
                            </a>
                            <article>
                                <div class="post-cotainer">
                                    <div class="post-head">
                                        <h2><a href="index.php#"><?= $value->post_title ?></a></h2>
                                        <p>автор: <?= $user_info->nickname; ?><span><?= get_date_from_gmt( $value->post_date, 'j F Y' ) ?></span></p>
                                    </div>
                                    <p><?= crop_string($value->post_content, 430, '...'); ?></p>
                                    <div class="post-foot">
                                        <ul class="social-share">
                                            <li>
                                                <span class="likebtn-wrapper" data-theme="youtube" data-lang="ru" data-identifier="item_<?= $id_post; ?>" data-site_id="57a0b32c9b1d1bea2cb765ce"></span>
                                            </li>
                                            <!-- 
                                            <li>
                                                <a href="blog.html#" class="vk"></a>
                                            </li>
                                            <li>
                                                <a href="blog.html#" class="facebook"></a>
                                            </li>
                                            <li>
                                                <a href="blog.html#" class="twitter"></a>
                                            </li> -->
                                        </ul>
                                        <a href="<?= get_permalink($id_post); ?>" class="more">читать далее <!-- LikeBtn.com BEGIN -->
<!-- LikeBtn.com END --></a>
                                    </div>
                                </div>
                                
                            </article>
                        </div>
                    <?php endforeach; ?>
            </div>
        </section>
<?php get_footer(); ?>
<script>(function(d,e,s){if(d.getElementById("likebtn_wjs"))return;a=d.createElement(e);m=d.getElementsByTagName(e)[0];a.async=1;a.id="likebtn_wjs";a.src=s;m.parentNode.insertBefore(a, m)})(document,"script","//w.likebtn.com/js/w/widget.js");</script>